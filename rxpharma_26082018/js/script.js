particlesJS('particles-js',
  
  {
    "particles": {
      "number": {
        "value": 80,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#FFF"
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#FFF"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.5,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 7,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 50,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 150,
        "color": "#FFF",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 6,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true    
  }

);

var acc = document.getElementsByClassName("frequently_child_plus");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {    
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

var s = $("#mainNav");
var pos = s.offset().top; //offset that you need is actually the div's top offset + it's height
$(window).scroll(function() {
    s.addClass('stick');        
    var windowpos = $(window).scrollTop(); //current scroll position of the window
    if(windowpos < 200){
        s.removeClass('stick');
    }
});

$('a.page-scroll').click(function(event){
  if (this.hash !== "") {
    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;
    
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 800, function(){       
      window.location.hash = hash;
    });
    $('a.page-scroll').removeClass('active');
    $(this).addClass('active');
  } 
});

AOS.init();

$(document).ready(function(){
    $('.tokens_child_nop1').tooltip({title: "<p>Founders and Advisors</p>", html: true, placement: "top"}); 
    $('.tokens_child_nop2').tooltip({title: "<p>Strategic partners</p>", html: true, placement: "top"}); 
    $('.tokens_child_nop3').tooltip({title: "<p>Bounty and marketing</p>", html: true, placement: "top"}); 
    $('.tokens_child_nop4').tooltip({title: "<p>Public Sale</p>", html: true, placement: "top"}); 
    $('.tokens_child_nop5').tooltip({title: "<p>Reward for customers</p>", html: true, placement: "top"}); 
    $('.tokens_child_nop6').tooltip({title: "<p>Reserved Liquidity Pool</p>", html: true, placement: "top"}); 
    $('.tokens_child_nop7').tooltip({title: "<p>Management team</p>", html: true, placement: "top"}); 
    $('.tokens_child_nop8').tooltip({title: "<p>Incentive for pharmacy</p>", html: true, placement: "top"}); 
    $('.tokens_child_nop9').tooltip({title: "<p>Early buyers with bonus</p>", html: true, placement: "top"}); 
});
 


var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 1500); // Change image every 2 seconds
}

var myIndex2 = 0;
carousel2();

function carousel2() {
    var i;
    var x = document.getElementsByClassName("mySlides2");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex2++;
    if (myIndex2 > x.length) {myIndex2 = 1}    
    x[myIndex2-1].style.display = "block";  
    setTimeout(carousel2, 2000); // Change image every 2 seconds
}

var myIndex3 = 0;
carousel3();

function carousel3() {
    var i;
    var x = document.getElementsByClassName("mySlides3");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex3++;
    if (myIndex3 > x.length) {myIndex3 = 1}    
    x[myIndex3-1].style.display = "block";  
    setTimeout(carousel3, 2500); // Change image every 2 seconds
}
